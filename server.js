global.BASE_DIR = __dirname

const http = require('http')
const express = require('express')
const config = require('./config')
const logger = require('./modules/logger')
const cors = require('cors')
const fileUpload = require('express-fileupload')
const { createProxyMiddleware } = require('http-proxy-middleware')
const path = require('path')

const app = express()
app.use(express.static(path.join(__dirname, 'public')))
app.use(cors())
app.use('/api/v1', express.json())
app.use('/api/v3', express.json())
app.use(express.urlencoded({extended: false}))
app.use('/api/v1', fileUpload())

app.use('/api/v1/login', require('./modules/auth').login)
// app.use('/api', require('./modules/auth').verify)

app.post('/api/v1/users', require('./modules/user').createUserMidWare)
app.use('/api/v1/csv', require('./modules/csv').router)
app.use('/api/v1/reports', require('./modules/report').router)

app.use('/api/v2', createProxyMiddleware({
  target: config.strapi.endpoint,
  changeOrigin: true,
  prependPath: false,
  pathRewrite: {
    '^/api/v2/products': '/wb-products',
    '^/api/v2/categories': '/wb-categories',
    '^/api/v2/related-products': '/wb-related-products',
    '^/api/v2/blogs': '/wb-blogs',
    '^/api/v2/related-blogs': '/wb-related-blogs',
    '^/api/v2/contact': '/wb-contacts',
    // '^/api/v2/inventories': '/pg-inventories',
    // '^/api/v2/lot-dates': '/pg-lot-dates',
    // '^/api/v2/competitors': '/pg-competitors',
    // '^/api/v2/gifts': '/pg-gifts',
    // '^/api/v2/upload': '/upload',
  },
  onProxyReq: (proxyReq, req, res) => {
    proxyReq.setHeader('Authorization', config.strapi.token)
  }
}))

app.use('/api/v3', require('./modules/multiple').router)

app.get('*', (req, res) => res.sendFile(path.join(__dirname, 'public', 'index.html')))

app.use((req, res) => {
  res.status(404).json({ message: `CANNOT ${req.method} API ${req.originalUrl}` })
})

app.use((err, req, res, _next) => res.status(500).json({ message: err.message || String(err) }))

const server = http.createServer(app)
server.listen(config.port.http, (err) => {
  if (err) {
    logger.error(`Start server error`, config, err)
  } else {
    logger.info(`Server is listening on port ${config.port.http}`)
  }
})
