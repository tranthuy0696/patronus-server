const axios = require('axios')
const config = require('../../config')

const options = {
    baseURL: config.strapi.endpoint,
    timeout: 5 * 60 * 1000,
    headers: {
        'Content-Type': 'application/json; charset=utf8',
        'Authorization': config.strapi.token,
    },
}
const client = axios.create(options)
module.exports = client
