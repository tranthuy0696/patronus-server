const logger = require('../logger')
const csvParser = require('csv-parse')

const doParse = (data) => new Promise((res, rej) => {
    csvParser(data, { columns: true }, (err, output) => {
        if (err) {
            logger.error(`Failed to parse to csv`, err)
            rej(err)
        } else {
            res(output)
        }
    })
})

const upload = async (req, res, next) => {
    let data
    try {
        const key = Object.keys(req.files)[0]
        data = req.files[key].data
        const output = await doParse(data)
        res.status(200).json(output)
    } catch (error) {
        logger.error('Failed to upload csv file: ', error)
        next(error)
    }
}

module.exports = {upload}
