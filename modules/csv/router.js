const express = require('express')
const router = new express.Router()
const handler = require('./handler')

router.post('/', handler.upload)

module.exports = router
