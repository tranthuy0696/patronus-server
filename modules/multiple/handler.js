const client = require('../client')

const create = (resource) => async (req, res, next) => {

    const createOne = async (data) => {
        try {
            const res = await client.post(resource, data)
            return res.data
        } catch (error) {
            logger.error(`Create ${resource} error`, error)
            return null
        }
    }
    try {
        const data = Array.isArray(req.body) ? req.body : [req.body]
        let newData = await Promise.all(data.map(createOne))
        newData = newData.filter((u) => u)
        res.status(200).json(newData)
    } catch (error) {
        next(error)
    }
}

module.exports = { create }
