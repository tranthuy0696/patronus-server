const express = require('express')
const router = new express.Router()
const handler = require('./handler')

router.post('/checkins', handler.create('pg-checkins'))
router.post('/outlets', handler.create('pg-outlets'))
router.post('/products', handler.create('pg-products'))
router.post('/product-groups', handler.create('pg-product-groups'))
router.post('/working-sheets', handler.create('pg-working-sheets'))
router.post('/sale-reports', handler.create('pg-sale-reports'))
router.post('/inventories', handler.create('pg-inventories'))
router.post('/lot-dates', handler.create('pg-lot-dates'))
router.post('/competitors', handler.create('pg-competitors'))
router.post('/gifts', handler.create('pg-gifts'))

module.exports = router
