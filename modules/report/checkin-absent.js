const client = require('../client')
const logger = require('../logger')
const checkinAbsent = async (req, res, next) => {
    try {
        let { date } = req.query
        date = Array.isArray(date) ? date : [date]

        const findByDate = async (singleDate) => {
            const start = new Date(Number.parseInt(singleDate))
            const end = new Date(Number.parseInt(singleDate))

            start.setHours(0, 0, 0, 0)
            end.setHours(0, 0, 0, 0)
            end.setDate(end.getDate() + 1)

            logger.info('checkin-absent logs', start, end, singleDate)

            const total = await client.get(`pg-working-sheets/count?startDate_gte=${start.getTime()}&startDate_lt=${end.getTime()}`)
                .then((res) => res.data)
                .catch((err) => {
                    logger.error('Failed to get total', err)
                    return 0
                })
            const actual = await client.get(`pg-checkins/count?startDate_gte=${start.getTime()}&startDate_lt=${end.getTime()}`)
                .then((res) => res.data)
                .catch((err) => {
                    logger.error('Failed to get actual', err)
                    return 0
                })
            return {checkin: actual, absent: Math.max(total - actual, 0)}
        }

        let result = await Promise.all(date.map(findByDate))
        res.status(200).json(result)
    } catch (error) {
        next(error)
    }
}

module.exports = checkinAbsent
