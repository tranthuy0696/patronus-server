const client = require('../client')

const getSaleReport = async (id) => {
    try {
        const res = await client.get(`pg-sale-reports/${id}`)
        return res.data
    } catch (error) {
        return null
    }
}

const listOutlets = async (month) => {
    const start = new Date()
    start.setTime(month)
    const end = new Date()
    end.setTime(month)

    start.setHours(0, 0, 0, 0)
    start.setDate(1)

    end.setHours(0, 0, 0, 0)
    end.setDate(1)
    end.setMonth(end.getMonth() + 1)

    let outlets = await client.get('pg-outlets').then((res) => res.data).catch(() => [])
    outlets = await Promise.all(outlets.map(async (u) => {
        let saleReportIds = u['pg_sale_reports']
            .filter((e) => {
                const date = new Date(e.createdAt)
                return date >= start && date < end
            }).map((e) => e.id)
        let saleReports = await Promise.all(saleReportIds.map(getSaleReport))
        saleReports = saleReports.filter((e) => e)
            .map((e) => e.itemCount * e.pg_product.price)
        u.actual = saleReports.reduce((a, b) => a + b, 0)
        u.target = u.target || 0
        return u
    }))
    return outlets
}

const targetActualGroup = async (req, res, next) => {
    try {
        const month = req.query.month
        let outlets = await listOutlets(month)
        let groups = {}
        outlets.forEach((u) => {
            if (groups[u.group]) {
                groups[u.group].actual += u.actual
                groups[u.group].target += u.target
            } else {
                groups[u.group] = {
                    target: u.target,
                    actual: u.actual,
                }
            }
        })

        const result = Object.keys(groups).map((group) => ({ group, actual: groups[group].actual, target: groups[group].target }))
        res.status(200).json(result)
    } catch (error) {
        next(error)
    }
}

module.exports = targetActualGroup
