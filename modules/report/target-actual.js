const client = require('../client')
const targetActual = async (req, res, next) => {
    try {
        let { date, month } = req.query
        const outlets = await client.get('pg-outlets').then((res) => res.data).catch(() => [])
        const target = outlets.map((u) => u.target || 0).reduce((a, b) => a + b, 0)
        if (date) {
            date = Array.isArray(date) ? date : [date]

            const findActualByDate = async (singleDate) => {
                const start = new Date()
                start.setTime(singleDate)
                const end = new Date()
                end.setTime(singleDate)

                start.setHours(0, 0, 0, 0)
                end.setHours(0, 0, 0, 0)
                end.setDate(end.getDate() + 1)
                let saleReports = await client.get(`pg-sale-reports?createdAt_gte=${start.getTime()}&createdAt_lt=${end.getTime()}`)
                    .then((res) => res.data).catch(() => [])
                saleReports = saleReports.map((e) => (e.itemCount || 0) * (e.pg_product.price || 0))
                const actual = saleReports.reduce((a, b) => a + b, 0)
                return actual
            }

            let result = await Promise.all(date.map(findActualByDate))
            result = result.map((e) => ({ target, actual: e }))
            res.status(200).json(result)
        } else {
            month = Array.isArray(month) ? month : [month]

            const findActualByMonth = async (singleMonth) => {
                const start = new Date()
                start.setTime(singleMonth)
                const end = new Date()
                end.setTime(singleMonth)

                start.setHours(0, 0, 0, 0)
                start.setDate(1)

                end.setHours(0, 0, 0, 0)
                end.setDate(1)
                end.setMonth(end.getMonth() + 1)

                let saleReports = await client.get(`pg-sale-reports?createdAt_gte=${start.getTime()}&createdAt_lt=${end.getTime()}`)
                    .then((res) => res.data).catch(() => [])
                saleReports = saleReports.map((e) => (e.itemCount || 0) * (e.pg_product.price || 0))
                const actual = saleReports.reduce((a, b) => a + b, 0)
                return actual
            }

            let result = await Promise.all(month.map(findActualByMonth))
            result = result.map((e) => ({ target, actual: e }))
            res.status(200).json(result)
        }
    } catch (error) {
        next(error)
    }
}

module.exports = targetActual
