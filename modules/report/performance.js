const client = require('../client')

const getSaleReport = async (id) => {
    try {
        const res = await client.get(`pg-sale-reports/${id}`)
        return res.data
    } catch (error) {
        return null
    }
}

const listOutlets = async () => {
    let outlets = await client.get('pg-outlets').then((res) => res.data).catch(() => [])
    outlets = await Promise.all(outlets.map(async (u) => {
        let saleReportIds = u['pg_sale_reports'].map((e) => e.id)
        let saleReports = await Promise.all(saleReportIds.map(getSaleReport))
        saleReports = saleReports.filter((e) => e)
            .map((e) => e.itemCount * e.pg_product.price)
        u.actual = saleReports.reduce((a,b) => a + b, 0)
        return u
    }))
    return outlets
}

const high = async (req, res, next) => {
    try {
        let { number } = req.query
        number = number || 5
        let outlets = await listOutlets()
        outlets.sort((a, b) => b.actual - a.actual)
        outlets = outlets.splice(0, number)
        res.status(200).json(outlets)
    } catch (error) {
        next(error)
    }
}

const low = async (req, res, next) => {
    try {
        let { number } = req.query
        number = number || 5
        let outlets = await listOutlets()
        outlets.sort((a, b) => a.actual - b.actual)
        outlets = outlets.splice(0, number)
        res.status(200).json(outlets)
    } catch (error) {
        next(error)
    }
}

module.exports = {high, low}
