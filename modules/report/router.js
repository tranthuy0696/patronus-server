const express = require('express')
const router = new express.Router()
const targetActual = require('./target-actual')
const checkinAbsent = require('./checkin-absent')
const performance = require('./performance')
const targetActualGroup = require('./target-actual-group')

router.get('/target-actual', targetActual)
router.get('/checkin-absent', checkinAbsent)
router.get('/top-high', performance.high)
router.get('/top-low', performance.low)
router.get('/target-actual-group', targetActualGroup)

module.exports = router
