const client = require('../client')
const logger = require('../logger')
const crypto = require('../crypto')

const getUsers = async (queries) => {
  try {
    let builtQueries = Object.keys(queries).map((key) => `${key}=${queries[key]}`).join('&')
    builtQueries = builtQueries ? `?${builtQueries}` : ''
    const res = await client.get(`pg-users${builtQueries}`)
    return res.data
  } catch (error) {
    logger.error('Get user error: ', error)
    return []
  }
}

const createUser = async (data) => {
  try {
    data.password = crypto.encrypt(String(data.password))
    const res = await client.post(`pg-users`, data)
    return res.data
  } catch (error) {
    logger.error('Create user error: ', error)
    return null
  }
}

const createUserMidWare = async (req, res, next) => {
  try {
    const users = Array.isArray(req.body) ? req.body : [req.body]
    let newUsers = await Promise.all(users.map((u) => createUser(u).catch(() => null)))
    newUsers = newUsers.filter((u) => u)
    newUsers.forEach((u) => delete u.password)
    res.status(200).json(newUsers)
  } catch (error) {
    next(error)
  }
}

module.exports = { getUsers, createUserMidWare }
